var controller;
$(document).ready(function(){
	//Öppnar alla externa länkar som nya fönster
	$("a[href^='http']").attr("target", "_blank");

	controller = new ScrollMagic();
	
	// var sceneDuration = (($(".contact").offset().top - $("#name").offset().top) - $("#name").height());

	// Delay animation to navbar and add animation.css class if desktop is used
	$('#nav').delay(3000).animate({opacity: "1.0"}, 1, function(){
        if (isDesktopWidth()) {
            $('#nav').queue(function(){$(this).addClass('animated flipInX')});    
        }
	});

    // Delay animation to name on header with animation.css
    $('#name').delay(2500).animate({opacity: "1.0"}, 1, function(){
        $('#name').queue(function(){$(this).addClass('animated rubberBand')});
    });

    // Delay animation to profile pic on header with animation.css
    $('#profileImage').delay(3200).animate({opacity: "1.0"}, 1, function(){
        $('#profileImage').queue(function(){$(this).addClass('animated pulse')});
    });

    // Create ScrollScene to make name in story appear on scroll
	new ScrollScene({triggerElement: "#trigger"})
					.setClassToggle(".storyName", "active") // add class toggle
					.addTo(controller);


    // Scroll smoothly on navbar clicks
    $("#goToContact").click(function() {
        scrollSmoothly("contact");
    });
    $("#goToStory").click(function() {
        scrollSmoothly("story");
    });
    $("#goToHeader").click(function() {
        scrollSmoothly("header");
    });
function scrollSmoothly(section) {
    $('html, body').animate({
            scrollTop: $("#"+section).offset().top
        }, 1600);
}
function isDesktopWidth() { 
    return $('.desktopIndicator').is(':visible');
}

});